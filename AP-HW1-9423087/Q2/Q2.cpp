/*full_name: Shamisa Kaspour
  student_num: 9423087*/

#include<iostream>
#include<iomanip>
#include<cmath>

double equation(double x);

int main()
{
  int cnt{1}; //counter for loop 
  double arr[3]{2, 1, 0}; //xn+1, xn & xn-1
  double error{1}; 
  std::cout << std::setw(3) << "cnt" << std::setw(13) << "x" << std::setw(13) 
	    << "error" << std::setw(13)  << "f(x)" << std::endl;
  while(error >= std::pow(10, -4)) //main loop for finding roots
  {
      arr[2] = (arr[0] * equation(arr[1]) - arr[1] * equation(arr[0])) 
	      		/ (equation(arr[1]) - equation(arr[0])); //xnew
      error = std::abs((arr[2] - arr[1]) / arr[1]);
      std::cout << std::setw(3) << cnt << std::setw(13) << arr[2] << std::setw(13) 
	      	<< error << std::setw(13)  << equation(arr[2]) 
	      	<< std::endl; // print cnt, f(x), x
      arr[0] = arr[1];
      arr[1] = arr[2];
      cnt++;
  }
  
  return 0;
}

double equation(double x) //for finding roots of this equation: f(x) = 0 
{
  // return std::pow(x, 3) + std::pow(x, 2) + x + 1;
   return 1/tanh(x) - log(x);
}
  
