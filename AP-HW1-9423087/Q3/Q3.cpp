/*full_name: Shamisa Kaspour
  student_num: 9423087*/
#include<iostream>

unsigned int lrfunc(int a, int b, int* arr);

int main()
{
  
  int num{}; //number of array elements
  unsigned int sigma{}; //total sigma (answer)
  
  std::cout << "please enter a number: ";
  std::cin >> num;
  int* arr_num{new int[num]}; //new dynamic array
  std::cout << "please enter values: ";
  for(int n{}; n < num; n++)
    std::cin >> arr_num[n]; //array elements
  
  for(int l{}; l <= num-1; l++) //l from 0 to num-1
    for(int r{l}; r <= num-1; r++) //r from l to num-1
      sigma += lrfunc(l, r, arr_num); //call f function for each l and r 
  
  std::cout << "output: " << sigma << std::endl; //print answer
  return 0;
}

unsigned int lrfunc(int a, int b, int* arr)
{

  unsigned int sigmap{0};
  
  for(; a <= b; a++)
    sigmap += arr[a];
  
  return sigmap;
}

  
