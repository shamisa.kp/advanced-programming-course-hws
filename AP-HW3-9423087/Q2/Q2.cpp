/*FullName: Shamisa Kaspour
  StudentNum: 9423087*/

#include <iostream>
#include "point.h"


int main() {
    /* show point test */
    point p1{0, 0};
    std::cout << "p1" << std::endl;
    p1.show();
    std::cout << std::endl;

    /* distance test */
    point p2{1, -2};
    std::cout << "p2" << std::endl;
    p2.show();
    point p3{-2, 2};
    std::cout << "p3" << std::endl;
    p3.show();
    std::cout << "distance(p2&p3): " << p2.distance(&p3) << std::endl;
    std::cout << std::endl;

    /* creating a line with a point */
    line l1{*(p2.Line(&p1))};
    std::cout << "l1(p2&p1)" << std::endl;
    l1.show();
    std::cout << std::endl;

    /* operator+ test */
    point p4{1, 3};
    std::cout << "p4" << std::endl;
    p4.show();
    point p5{p4.operator+(p2)};
    std::cout << "p5= operator+(p4&p2)" << std::endl;
    p5.show();
    std::cout << std::endl;

    /* line */
    line l2{&p1, &p5};
    std::cout << "l2" << std::endl;
    l2.show();
    std::cout << std::endl;

    /* parallel lines test */
    std::cout << "(l2&l1)NOT parallel :" << l2.isParallel(&l1) << std::endl;
    std::cout << std::endl;

    /* perpendicular lines test */
    std::cout << "(l2&l1) prependicular :" << l2.isPrependicular(&l1) << std::endl;
    std::cout << std::endl;

    /* intersection test */
    point p6{2, 0};
    line l3{&p4, &p6};
    std::cout << "l3" << std::endl;
    l3.show();
    std::cout << "intersection point(l3&l1)" << std::endl;
    l3.intersection(&l1)->show();
    std::cout << std::endl;

    /* creating a parallel line */
    std::cout << "parallel line with a point(l3&p3)" << std::endl;
    l3.parallel(&p3)->show();
    std::cout << std::endl;


    return 0;
}