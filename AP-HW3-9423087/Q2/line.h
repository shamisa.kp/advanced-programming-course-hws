/*FullName: Shamisa Kaspour
  StudentNum: 9423087*/

#ifndef Q2_LINE_H
#define Q2_LINE_H
class point;  //avoid compiling error


class line {
public:
    line(point*, point*);  //constructor
    line() = default;  //default constructor
    bool isParallel(line*);
    bool isPrependicular(line*);
    point* intersection(line*);
    line* parallel(point*);
    void show();

private:
    double slope;  //slope of line
    int xline;  //x value of initialization point for line
    int yline;  //y value of initialization point for line
    point* intersectionp;  // intersection point in it's scope
    point* parallelp;  //parallel point in it's scope
};


#endif //Q2_LINE_H