/*FullName: Shamisa Kaspour
  StudentNum: 9423087*/

#include "vector.h"
#include <iostream>

/* constructor definition */
Vector::Vector() {
    elements = 0;
    vecPointer = nullptr;
};

/* function to push back */
void Vector::push_back(int value) {
    elements++; //now element is increased
    int* pointer{new int[elements - 1]};  //pointer to store elements of vecpointer
    for (int i = 0; i < elements - 1  ; ++i) {
        pointer[i] = vecPointer[i];  //copy elements of vecpointer in pointer
    }
    vecPointer = new int[elements];
    vecPointer[elements - 1] = value; //store new value
    for (int j{0}; j < elements - 1 ; j++) {
        vecPointer[j] = pointer[j];  //restore elements to vecpointer
    }
    delete[] pointer;
}

/* function to display elements */
void Vector::display() {
    std::cout << "elements of Vector:" << std::endl;
        for (int i{0}; i < elements; i++)
            std::cout << "v" << i+1 << ": " << *(vecPointer + i) << std::endl;
}

/* function to pop back */
void Vector::pop_back() {
    --elements;
}

/* function to display an element of a vector */
void Vector::Subvec(int sv) {
    std::cout << "v" << sv << ": " << *(vecPointer + (sv - 1)) << std::endl;
}

/* destructor definition */
Vector::~Vector() {
    delete[] vecPointer;
    vecPointer = nullptr;
}

/* copy constructor definition */
Vector::Vector(Vector &copy) {
		elements = copy.elements;
    vecPointer = new int[elements];
    for (int i{0}; i < elements; i++)
        vecPointer[i] = copy.vecPointer[i];
}

/* operator + */
Vector Vector::operator+(Vector& v) {
    Vector newV{};  //a temporary vector to store all elements
    newV.vecPointer = new int[elements + v.elements];
    newV.elements = elements + v.elements;
        for (int i{0}; i < elements; i++) {
            *(newV.vecPointer + i) = *(this->vecPointer + i);
        }
        for (int j{elements}; j < elements + v.elements ;j++) {
            *(newV.vecPointer + j) = *(v.vecPointer + (j - elements));
        }
        return newV;
    }

/* operator = */
Vector& Vector::operator=(const Vector& v) {
    if(this == &v) {
        return *this;
    }
    else {
        delete[] this->vecPointer;
        this->vecPointer = new int[v.elements];
        this->elements = v.elements;
        for (int i{0}; i < v.elements; i++) {
            *(this->vecPointer + i) = *(v.vecPointer + i);
        }
        return *this;
    }
}

/* function to return size of a vector */
int Vector::size() {
    return elements;
}

/* function to return max element of a vector */
int Vector::max() {
    int max{};
    max = vecPointer[0];
    for (int i{0}; i < elements ; i++) {
        if(max < vecPointer[i])
            max = vecPointer[i];
    }
    return max;
}

