/*FullName: Shamisa Kaspour
  StudentNum: 9423087*/

#include <iostream>
#include "point.h"
#include "polygon.h"
#include <cmath>

int main() {
    point p0{0, 0};
    point p1{0, 1};
    point p2{1, 1};
    point p3{1, 0};
//    point p4{-1, 0};
//    point p5{-1, 1};
//    point p6{0, 1};
//    point p7{0, 0};

//    point p2{3, 2}; // Checking is Square
    line l0{&p0, &p1};
    line l1{&p1, &p2};
    line l2{&p2, &p3};
    line l3{&p3, &p0};
    point* pArr{new point[4]{p0, p1, p2, p3}};
    line* lArr{new line[4]{l0, l1, l2, l3}};

    /* Square */
    polygon polygon1{pArr, 4};
    polygon polygon2{lArr, 4};
    /* check is equal */
//    point* Pnew{new point[4]{p7, p6, p5, p4}};
//    polygon polygon4{Pnew, 4};

    /* Triangle */
    delete[] pArr;
    p2.setXpos(1);
    p2.setYpos(0);
    pArr = new point[3]{p0, p1, p2};
    polygon polygon3{pArr, 3};
//    std::cout << polygon1.isEqual(polygon4) << std::endl;
    /* show polygon */
//    polygon1.show();
//    polygon2.show();
//    polygon3.show();
//
    std::cout<< polygon1.isSquare() << std::endl;
    std::cout<< polygon3.isTriangle() << std::endl;

    delete[] pArr;
    delete[] lArr;
//  delete[] Pnew;

    return 0;
}