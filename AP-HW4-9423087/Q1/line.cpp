/*FullName: Shamisa Kaspour
  StudentNum: 9423087*/

#include "line.h"
#include "point.h"
#include <limits>
#include <iostream>

/* constructor definition */
line::line(point* a, point* b) {
    xline = a->getX();
    yline = a->getY();

    if(a->getX()- b->getX() == 0) { //if slope is infinity
        slope = std::numeric_limits<double>::infinity();
    }
    else { //slope of line
        slope = static_cast<double>(b->getY() - a->getY()) /
                static_cast<double>(b->getX() - a->getX());
    }
}

bool line::isParallel(line* newl) {
    return this->slope == newl->slope;
}

bool line::isPrependicular(line* newl) {
    double infinity{std::numeric_limits<double>::infinity()};
    if(this->slope == infinity && newl->slope == 0
        || this->slope == 0 && newl->slope == infinity)
        return true;
    else {
        return this->slope * newl->slope == -1;
    }

}

point* line::intersection(line* newl) {
    if(!isParallel(newl)) {
        int newx{0};  //intersection point x&y
        int newy{0};
        if(this->slope == std::numeric_limits<double>::infinity()) {
            newx = this->xline;
            newy = static_cast<int>(newl->slope * newx - newl->slope
                                                         * newl->xline + newl->yline);
        }
        else if( newl->slope == std::numeric_limits<double>::infinity()) {
            newx = newl->xline;
            newy = static_cast<int>(this->slope * newx - this->slope
                                                         * this->xline + this->yline);
        }
        else {
            newx = static_cast<int>(1 / (this->slope - newl->slope) *
                                    (newl->yline - newl->slope * newl->xline - this->yline
                                     + this->slope * this->xline));
            newy = static_cast<int>(newl->slope * newx +
                                    newl->yline - newl->slope * newl->xline);
        }
        intersectionp = new point(newx, newy);
        return intersectionp;
    }
    else {
        std::cout<< " these lines are parallel! " << std::endl;
        return nullptr;
    }
}
/*parallel line with a given point*/
line* line::parallel(point* newp) {
    double infinity{std::numeric_limits<double>::infinity()};
    if(this->slope * (xline - newp->getX()) + newp->getY()
        - this->yline != 0) {
        if(this->slope == infinity) {
            parallelp = new point(newp->getX(), (newp->getY()) + 1);
            line parralell{newp, parallelp};
            line *ptoline{&parralell};
            return ptoline;
        } else {
            parallelp = new point(newp->getX() + 1, static_cast<int>
            (newp->getY() + 1 * this->slope));
            line parralell{newp, parallelp};
            line *ptoline{&parralell};  //pointer to line
            return ptoline;
        }
    } else return this; // it is a point of line
}

/*print line */
void line::show() {
    if(slope == std::numeric_limits<double>::infinity())
        std::cout << "x: " << xline << std::endl;
    else if(slope == 0)
        std::cout << "y: " << yline << std::endl;
    else
        std::cout<< "y = " << slope << " * x + (" << (slope * (-xline) + yline)
                 << ")" << std::endl;
}


double line::getSlope() {
    return slope;
}
