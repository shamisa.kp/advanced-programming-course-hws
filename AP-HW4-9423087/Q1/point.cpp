/*FullName: Shamisa Kaspour
  StudentNum: 9423087*/

#include "point.h"
#include <cmath>
#include <iostream>

/* constructor definition */
point::point(int x, int y) {
    xpos = x;
    ypos = y;
}

/* function to get x */
int point::getX() {
    return xpos;
}

/* function to get y */
int point::getY() {
    return ypos;
}

/* function to return distance between 2 points */
double point::distance(point* p) {  //return distance
    return sqrt(pow(xpos - p->xpos, 2) + pow(ypos - p->ypos, 2));
}
/* equation of a line from 2 points */
line* point::Line(point* p) {
    if((this->getX() == p->getX()) && (this->getY() == p->getY())) {
        std::cout << "it's the same point!" << std::endl;
        return nullptr;
    }
    else {
        pointl = new line{this, p}; //create a new pointer to object of line
        return pointl;
    }
}

/* plus 2 points */
point point::operator+(point& p) {
    point totalp{xpos + p.xpos, ypos + p.ypos}; //create a new object of point
    return totalp;
}

/* print point */
void point::show() {
    std::cout << "x: " << xpos << " y: " << ypos << std::endl;
}

void point::setXpos(int xpos) {
    point::xpos = xpos;
}

void point::setYpos(int ypos) {
    point::ypos = ypos;
}

