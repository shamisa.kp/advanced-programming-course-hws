 //
// Created by shamisa on 4/9/18.
//
#include <iostream>
#include <limits>
#include <cmath>
#include "polygon.h"

 /* constructor to create an object of polygon out of input points */
 polygon::polygon(point *arr, int size) {
     N = size;  //number of points
     if(N <= 2) {
         std::cout << "the size of array to create an "
                      "object of polygon is not enough" << std::endl;
         return;
     }

     vertex = new point[size];  //creating an array to copy input points
     for(int i{0}; i < size; i++) {
         vertex[i] = arr[i];
     }

     side = new line[size];   //making lines out of input points
     for(int i{0}; i < size - 1; i++) {
         side[i] = *(vertex[i].Line(&vertex[i + 1]));
     }
     side[size - 1] = *(vertex[size - 1].Line(&vertex[0]));

     /* finding three points in a line */
     for(int i{}; i < N - 1; i++) {
         if(side[i].isParallel(&side[i + 1])) {
             std::cout << "Can not make a polygon with these "
                          "lines or points!" << std::endl;
             return;
         }
     }
     if(side[N - 1].isParallel(&side[0])) {
         std::cout << "Can not make a polygon with these "
                      "lines or points!" << std::endl;
         return;
     }

     /* isEqual */
     setAngles();
     for(int i{0}; i < N - 1; i++) {
        if(vertex[i].distance(&vertex[i + 1]) == 0) {
            std::cout << "Same Line!" << std::endl;
            return;
        }
     }

     if(vertex[N-1].distance(&vertex[0]) == 0) {
         std::cout << "Same Line!" << std::endl;
         return;
     }
 }

/* constructor to create an object of polygon out of input lines */
 polygon::polygon(line *arr, int size) {
     N = size;  //number of lines
     if(N <= 2) {
         std::cout << "the size of array to create an "
                      "object of polygon is not enough" << std::endl;
         return;
     }

     side = new line[size];
     for (int i{0}; i < size; i++) {
         side[i] = arr[i];
     }

     /* finding two or more equal lines */
     for(int i{}; i < N - 1; i++) {
         if(side[i].isParallel(&side[i + 1])) {
             std::cout << "Can not make a polygon with these "
                          "lines or points!" << std::endl;
             return;
         }
     }
     if(side[N - 1].isParallel(&side[0])) {
         std::cout << "Can not make a polygon with these "
                      "lines or points!" << std::endl;
         return;
     }

     /* isEqual */
     setAngles();

     vertex = new point[size];  //making vertexes out of input lines
     for(int y{0}; y < size; y++)
         vertex[y] = *(side[y].intersection(&side[(y + 1 == size ? 0 :
                                                   y + 1)]));

     for(int i{0}; i < N - 1; i++) {
         if(vertex[i].distance(&vertex[i + 1]) == 0) {
             std::cout << "Same Line!" << std::endl;
             return;
         }
     }

     if(vertex[N-1].distance(&vertex[0]) == 0) {
         std::cout << "Same Line!" << std::endl;
         return;
     }
 }

 /*if polygon is a square */
 bool polygon::isSquare() {
     if(N == 4) {
         if(side[0].isParallel(&side[2]) && side[1].isParallel(&side[3]) &&
             side[0].isPrependicular(&side[1])) {
             if(vertex[0].distance(&vertex[1]) == vertex[1].
                     distance(&vertex[2])) {
                 return true;
             }
         }
     }
     return false;
 }

/* if polygon is a triangle */
bool polygon::isTriangle() {
     return N == 3;
}

/* destructor */
 polygon::~polygon() {
    delete[] vertex;
    delete[] side;
    delete[] angle;
 }

 /* copy constructor */
 polygon::polygon(polygon & p) {
     this->N = p.N;
     this->side = new line[N];
     this->vertex = new point[N];
     this->angle = new double[N];
     for (int i{0}; i < N; i++) {
         this->vertex[i] = p.vertex[i];
         this->side[i] = p.side[i];
         this->angle[i] = p.angle[i];
     }
 }

 /* operator= */
 polygon& polygon::operator=(const polygon &p) {
     if(this == &p)
         return *this;
     else {
         delete[] vertex;
         delete[] side;
         delete[] angle;
         this->N = p.N;
         side = new line[p.N];
         vertex = new point[p.N];
         angle = new double[p.N];
         for(int i{}; i < p.N; i++){
             this->vertex[i] = p.vertex[i];
             this->side[i] = p.side[i];
             this->angle[i] = p.angle[i];
         }
         return *this;
     }
 }

 void polygon::show() {
     std::cout << "Points: " << std::endl;
    for(int i{}; i < N; i++) {
        vertex[i].show();
    }

    std::cout << "Lines: " << std::endl;
     for(int i{}; i < N; i++) {
         side[i].show();
     }
 }

void polygon::setAngles() {
    angle = new double[N];
    for (int j{0}; j < N - 1; j++) {
        if(side[j].getSlope() == std::numeric_limits<double>::infinity())
            angle[j] = 90 - fabs(atan(side[j + 1].getSlope()) * 180 / M_PI);
        else if(side[j + 1].getSlope() == std::numeric_limits<double>::infinity())
            angle[j] = 90 - fabs(atan(side[j].getSlope()) * 180 / M_PI);
        else
            angle[j] = atan(fabs((side[j + 1].getSlope() - side[j].getSlope())
                                 / (1 + side[j + 1].getSlope()
                                        * side[j].getSlope()))) * 180 / M_PI;
    }
    if(side[0].getSlope() == std::numeric_limits<double>::infinity())
        angle[N - 1] = 90 - fabs(atan(side[N - 1].getSlope()) * 180 / M_PI);
    else if(side[N - 1].getSlope() == std::numeric_limits<double>::infinity())
        angle[N - 1] = 90 - fabs(atan(side[0].getSlope()) * 180 / M_PI);
    else
        angle[N - 1] = atan(fabs(
                (side[0].getSlope() - side[N - 1].getSlope()) /
                (1 + side[0].getSlope() * side[N - 1].getSlope()))) * 180 / M_PI;
}


 bool polygon::isEqual(polygon p) {
     if(N != p.N) {
         return false;
     }
     int iterator{-1};
     for(int i{0}; i < N; i++) {
         if(angle[0] == p.angle[i]) {
             iterator = i;
             break;
         }
     }
     if(iterator == -1)
         return false;

     int index{iterator};
     for (int i{0}; i < N; i++) {
         if(angle[i] != p.angle[iterator++])
             return false;
         if(iterator == N)
             iterator = 0;
     }

     for(int j{0}; j < N - 1; j++) {
         if(index == N - 1) {
             if(vertex[j].distance(&vertex[j + 1]) != p.vertex[N - 1].distance(&p.vertex[0]))
                 return false;
             index = 0;
         }
         if(vertex[j].distance(&vertex[j + 1]) != p.vertex[index++].distance(&p.vertex[index + 1]))
             return false;
     }
     if(index == N - 1)
         return vertex[N - 1].distance(&vertex[0]) == p.vertex[N - 1].distance(&p.vertex[0]);
     return vertex[N - 1].distance(&vertex[0]) == p.vertex[index].distance(&p.vertex[index + 1]);
 }
