//
// Created by shamisa on 4/9/18.
//

#ifndef Q1_POLYGON_H
#define Q1_POLYGON_H
#include"line.h"
#include "point.h"

class polygon {
public:
    polygon(point* arr, int size);
    polygon(line* arr, int size);
    ~polygon();
    polygon(polygon&);
    polygon& operator=(const polygon& p);
    bool isTriangle();
    bool isSquare();
    bool isEqual(polygon p);
    void show();
private:
    void setAngles();
    point* vertex;  // vertexes of a polygon
    line* side;  //sides of a polygon
    double* angle;
    int N;  //size of input array
};


#endif //Q1_POLYGON_H
