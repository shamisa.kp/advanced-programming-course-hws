//
// Created by shamisa on 4/14/18.
//

#include "Shape.h"

/* function to calculate area of a shape */
double Shape::area() {
    return 0;
}

/* function to print */
std::ostream& Shape::print(std::ostream& input) {
    return input;
}

/* operator<< */
std::ostream &operator<<(std::ostream &input, Shape &shape) {
    return shape.print(input);
}

