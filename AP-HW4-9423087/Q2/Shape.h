//
// Created by shamisa on 4/14/18.
//

#ifndef Q2_SHAPE_H
#define Q2_SHAPE_H


#include <ostream>
#include "point.h"

class Shape {
public:
    Shape() = default;  // default constructor
    virtual double area();
    virtual std::ostream &print(std::ostream& input);
    friend std::ostream& operator<<(std::ostream& input, Shape& shape);

protected:
    double x{};  // x & y of a centre point of a shape
    double y{};

};


#endif //Q2_SHAPE_H
