//
// Created by shamisa on 4/14/18.
//

#ifndef Q2_THREEDIMENSIONALSHAPE_H
#define Q2_THREEDIMENSIONALSHAPE_H


#include "Shape.h"

class ThreeDimensionalShape: public Shape{
public:
    ThreeDimensionalShape() = default;
    virtual double volume();
protected:
    double z{};  //z of ThreeDimensionalShape
};


#endif //Q2_THREEDIMENSIONALSHAPE_H
