//
// Created by shamisa on 4/14/18.
//

#ifndef Q2_TWODIMENSIONALSHAPE_H
#define Q2_TWODIMENSIONALSHAPE_H


#include "Shape.h"

class TwoDimensionalShape: public Shape{
public:
    TwoDimensionalShape() = default;
};


#endif //Q2_TWODIMENSIONALSHAPE_H
