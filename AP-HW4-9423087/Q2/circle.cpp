//
// Created by shamisa on 4/14/18.
//

#include "circle.h"
#include <cmath>
#include <iostream>

/* constructor */
Circle::Circle(double r, double x, double y) {
    if(r <= 0)
        std::cout << "radius can not be negative!!" <<std::endl;
    radius = r;
    this->x = x;
    this->y = y;
}

/* area */
double Circle::area() {
    return M_PI * radius * radius;
}


/* print */
std::ostream& Circle::print(std::ostream &input) {
    input << "Circle radius = " << radius << std::endl
          << "center --> " << "(" << this->x
          << ", " << this->y << ")" << std::endl
          << "area of " << area() << std::endl << std::endl;
    return input;
}

/* operator+ on centre point of a circle with another point */
Circle Circle::operator+(point &p) {
    return {radius, this->x + p.getX(), this->y + p.getY()};
}


