//
// Created by shamisa on 4/14/18.
//

#ifndef Q2_CIRCLE_H
#define Q2_CIRCLE_H


#include "TwoDimensionalShape.h"

class Circle: public TwoDimensionalShape {
public:
    Circle(double r, double x, double y);
    double area() override;
    std::ostream& print(std::ostream& input) override;
    Circle operator+(point& p);

private:
    double radius;

};


#endif //Q2_CIRCLE_H
