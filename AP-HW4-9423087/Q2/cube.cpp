//
// Created by shamisa on 4/14/18.
//

#include "cube.h"
#include <iostream>

/* definition of constructor */
Cube::Cube(double l, double x, double y, double z) {
    if(l <= 0)
        std::cout << "length can not be negative!!" << std::endl;
    lCube = l;
    this->x = x;
    this->y = y;
    this->z = z;
}

/* volume of cube */
double Cube::volume() {
    return lCube * lCube * lCube;
}

/* area of cube */
double Cube::area() {
    return 6 * lCube * lCube;
}

/* print function */
std::ostream &Cube::print(std::ostream &input) {
    input << "Cube side length = " << lCube << std::endl
          << "center --> " << "(" << this->x
          << ", " << this->y << ", " << this->z <<")" << std::endl
          << "area of " << area() << " & volume of "
                                     << volume() << std::endl << std::endl;
    return input;
}

/*/* operator+ on centre point of cube with another point */
Cube Cube::operator+(point& p) {
    return Cube(lCube, this->x + p.getX(), this->y + p.getY(), this->z);
}
