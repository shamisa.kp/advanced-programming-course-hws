//
// Created by shamisa on 4/14/18.
//

#ifndef Q2_CUBE_H
#define Q2_CUBE_H


#include <cmath>
#include <ostream>
#include "ThreeDimensionalShape.h"

class Cube: public ThreeDimensionalShape {
public:
    explicit Cube(double l, double x = 0, double y = 0, double z = 0);
    double area() override;
    double volume() override;
    std::ostream& print(std::ostream& input) override;
    Cube operator+(point& p);

private:
    double lCube;  //length of cube
};


#endif //Q2_CUBE_H
