/*FullName: Shamisa Kaspour
  StudentNum: 9423087*/

#include <iostream>
#include "circle.h"
#include "square.h"
#include "sphere.h"
#include "cube.h"
#include "Shape.h"
#include "square.h"

using std::cout;

int main() {
    Square sqr(12, 2, 2);
    Sphere sph(5, 1.5, 4.5);
    Cube cub(2.2);
    Circle cir(3.5, 6, 9);
    Shape *ptr[4]{&cir, &sqr, &sph, &cub};
    for(int x{}; x < 4; x++) {
        cout << *(ptr[x]) << '\n';
    }

//    /* operator+ */
//    point p{2, 7};
//    Circle newCir{cir + p};
//    Square newSqr{sqr + p};
//    Cube newCub{cub + p};
//    Sphere newSph{sph + p};
//    std::cout << newCir << newSqr << newSph << newCub;
    return 0;
}


