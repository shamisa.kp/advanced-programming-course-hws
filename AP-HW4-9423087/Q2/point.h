/*FullName: Shamisa Kaspour
  StudentNum: 9423087*/

#ifndef Q2_POINT_H
#define Q2_POINT_H
#include "line.h"


class point {
public:
    point(int x, int y);
    point() = default;
    int getX();
    int getY();
    int distance(point*);
    line* Line(point*);
    point operator+(point&);
    void show();

private:
    int xpos;   //x&y position for point
    int ypos;
    line* pointl;  //pointer to line created with a point
};


#endif //Q2_POINT_H