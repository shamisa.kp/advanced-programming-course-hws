//
// Created by shamisa on 4/14/18.
//

#include "sphere.h"
#include <cmath>
#include <iostream>

/* constructor definition */
Sphere::Sphere(double r, double x, double y, double z) {
    if(r <= 0)
        std::cout << "radius can not be negative!!" <<std::endl;
    rSphere = r;
    this->x = x;
    this->y = y;
    this->z = z;
}

/* volume of sphere */
double Sphere::volume() {
    return (4./3) * M_PI * rSphere * rSphere * rSphere;
}

/* area of  sphere */
double Sphere::area() {
    return 4 * M_PI * rSphere * rSphere;
}

/* print function */
std::ostream& Sphere::print(std::ostream &input) {
    input << "Sphere radius = " << rSphere << std::endl
          << "center --> " << "(" << this->x
          << ", " << this->y << ", " << this->z <<")" <<std::endl
          << "area of " << area() << " & volume of "
                                     << volume() << std::endl << std::endl;
    return input;
}

/* operator+ on centre point of sphere with another point */
Sphere Sphere::operator+(point &p) {
    return {rSphere, this->x + p.getX(), this->y + p.getY(), this->z};
}

