//
// Created by shamisa on 4/14/18.
//

#ifndef Q2_SPHERE_H
#define Q2_SPHERE_H


#include "ThreeDimensionalShape.h"
#include <ostream>

class Sphere: public ThreeDimensionalShape {
public:
    Sphere(double r, double x, double y, double z = 0);
    double area() override;
    double volume() override;
    std::ostream& print(std::ostream& input) override;
    Sphere operator+(point& p);

private:
    double rSphere;  // radius of sphere
};


#endif //Q2_SPHERE_H
