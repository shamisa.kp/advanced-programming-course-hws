//
// Created by shamisa on 4/14/18.
//

#include "square.h"
#include <iostream>

/* constructor definition of a square */
Square::Square(double l, double x, double y) {
    if(l <= 0)
        std::cout << "length can not be negative!!" << std::endl;
    this->lSquare = l;
    this->x = x;
    this->y = y;
}

/* area of a square */
double Square::area() {
    return lSquare * lSquare;
}

/* print function */
std::ostream &Square::print(std::ostream &input) {
    input << "Square side length = " << lSquare << std::endl
          << "center --> " << "(" << this->x
          << ", " << this->y << ")" << std::endl
          << "area of " << area() << std::endl << std::endl;
    return input;
}

/* operator+ on centre point of a square with another point */
Square Square::operator+(point &p) {
    return {lSquare, this->x + p.getX(), this->y + p.getY()};
}


