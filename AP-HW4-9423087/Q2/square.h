//
// Created by shamisa on 4/14/18.
//

#ifndef Q2_SQUARE_H
#define Q2_SQUARE_H


#include "TwoDimensionalShape.h"

class Square: public TwoDimensionalShape {
public:
    Square(double l, double x, double y);
    double area() override;
    std::ostream& print(std::ostream& input) override;
    Square operator+(point& p);

private:
    double lSquare;  //length of square
};


#endif //Q2_SQUARE_H
