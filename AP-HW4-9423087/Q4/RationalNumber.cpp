//
// Created by shamisa on 4/14/18.
//

#include "RationalNumber.h"

/* definition of constructor */
RationalNumber::RationalNumber(int n, int d) {
    if(d <= 0) {  //negative or zero denominator!
        std::cout << "improper denominator!" << std::endl;
        return;
    }
    if(n == 0) {
        numerator = 0;
        denominator = 1;
        return;
    }

    /* absolute min between d & n */
    int min{(abs(d) < abs(n)) ? abs(d) : abs(n)};
    bool condition{false};  // bool variable to initialize denominator
    // & numerator when they haven't initialized
    for(int i{min}; i > 1; i--) {  //finding binary gcd
        if((d % i == 0) && (n % i == 0)) {
            denominator = d / i;
            numerator = n / i;
            condition = true;
            break;
        }
    }
    if(!condition) {
        denominator = d;
        numerator = n;
    }
}

/* definition of operator+ */
RationalNumber RationalNumber::operator+(RationalNumber& n) {
    if(this->denominator == n.denominator)  // when denominators are equal
        return {this->numerator + n.numerator, n.denominator};
    else
        return {this->numerator * n.denominator
                            + n.numerator * this->denominator,
                            this->denominator * n.denominator};
}

/* function to show a fraction */
void RationalNumber::show() {
    if((numerator == 0) || (denominator == 1))
        std::cout << numerator;
    else
        std::cout << numerator << "/" << denominator;
}

/* definition of operator- */
RationalNumber RationalNumber::operator-(RationalNumber & n) {
    if(this->denominator == n.denominator)
        return {this->numerator - n.numerator, n.denominator};
    else
        return {this->numerator * n.denominator
                - n.numerator * this->denominator,
                this->denominator * n.denominator};

}

/* definition of operator/ */
RationalNumber RationalNumber::operator/(RationalNumber & n) {
    if(n.numerator == 0) {
        std::cout << "denominator is 0 and it's not acceptable!" << std::endl;
        return n;
    }
    else
        return {this->numerator * n.denominator, this->denominator * n.numerator};
}

/* definition of operator* */
RationalNumber RationalNumber::operator*(RationalNumber& n) {
    return {this->numerator * n.numerator, this->denominator * n.denominator};
}

/* definition of operator== */
bool RationalNumber::operator==(RationalNumber& n) {
    return (this->numerator == n.numerator) && (this->denominator == n.denominator);
}

/* definition of operator> */
bool RationalNumber::operator>(RationalNumber& n) {
    return ((numerator * n.denominator) > (n.numerator * denominator));
}

/* definition of operator!= */
bool RationalNumber::operator!=(RationalNumber& n) {
    return !operator==(n);
}

/* definition of operator< */
bool RationalNumber::operator<(RationalNumber& n) {
    return !(operator==(n) || operator>(n));
}

/* definition of operator<= */
bool RationalNumber::operator<=(RationalNumber& n) {
    return !operator>(n);
}

/* definition of operator>= */
bool RationalNumber::operator>=(RationalNumber& n) {
    return !operator<(n);
}

