//
// Created by shamisa on 4/14/18.
//

#ifndef Q4_RATIONALNUMBER_H
#define Q4_RATIONALNUMBER_H

#include <iostream>
class RationalNumber {
public:
    RationalNumber(int n, int d);
    RationalNumber() = default;  //default constructor
    RationalNumber operator+(RationalNumber& n);
    RationalNumber operator*(RationalNumber& n);
    RationalNumber operator/(RationalNumber& n);
    RationalNumber operator-(RationalNumber& n);
    bool operator<=(RationalNumber& n);
    bool operator>=(RationalNumber& n);
    bool operator==(RationalNumber& n);
    bool operator!=(RationalNumber& n);
    bool operator<(RationalNumber& n);
    bool operator>(RationalNumber& n);
    void show();

private:
    int denominator{1};
    int numerator{0};

};


#endif //Q4_RATIONALNUMBER_H
