//
// Created by shamisa on 5/6/18.
//

#include "Test.h"
#include<iostream>
#include <utility>

/* Constructor Definition */
Test::Test(std::string s) {
    str = std::move(s);
}

/* function to show the sring */
void Test::display() {
    std::cout << str << " DISPLAY" << std::endl;
}

/* get string of test's object */
std::string Test::getString() {
    return str;
}
