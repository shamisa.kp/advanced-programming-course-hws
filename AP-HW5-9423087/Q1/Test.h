//
// Created by shamisa on 5/6/18.
//

#ifndef Q1_TEST_H
#define Q1_TEST_H
#include <string>

class Test {
public:
    explicit Test(std::string s);  //constructor
    std::string getString();  //return string
    void display();  //print string

private:
    std::string str;

};


#endif //Q1_TEST_H
