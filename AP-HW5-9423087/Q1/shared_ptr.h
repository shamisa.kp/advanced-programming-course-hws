//
// Created by shamisa on 5/6/18.
//

#ifndef Q1_SHARED_PTR_H
#define Q1_SHARED_PTR_H

#include "Test.h"

template<class T>
class shared_ptr {
public:
    explicit shared_ptr(T* p);  //constructor
    int use_count();  //count pointers of a object
    ~shared_ptr();  //destructor
    shared_ptr(shared_ptr &copy); //copy constructor
    Test* operator->();

private:
    T *pointer;  // pointer to object in constructor
    int* counter;  //pointer to number of pointers to one object

};

#include "shared_ptr.hpp"

#endif