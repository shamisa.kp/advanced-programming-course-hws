//
// Created by shamisa on 5/6/18.
//
#include "shared_ptr.h"
#include <iostream>


/* constructor definition */
template<class T>
shared_ptr<T>::shared_ptr(T* p) {
    counter = new int{1};
    pointer = p;
    std::cout << "Constructor " << p->getString() << std::endl;
}

/* use_count function */
template<class T>
int shared_ptr<T>::use_count(){
    return *counter;
}

/*destructor definiton */
template<class T>
shared_ptr<T>::~shared_ptr() {
    if (use_count() == 1) {
        // print destructor
        std::cout << "Destructor " << pointer->getString() << std::endl;
        delete pointer;
        delete counter;

    }
    else
        --(*counter);
}

/* copy constructor */
template<class T>
shared_ptr<T>::shared_ptr(shared_ptr &copy) {
    counter = copy.counter;
    pointer = copy.pointer;
    ++(*counter);  //equalization the number of pointers
}

/* arrow operator */
template<class T>
Test* shared_ptr<T>::operator->() {
    return this->pointer;
}


