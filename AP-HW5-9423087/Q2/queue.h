//
// Created by shamisa on 5/11/18.
//

#ifndef Q2_QUEUE_H
#define Q2_QUEUE_H



template<class T>
class Queue {
    class item {
    public:
        T itemValue;
        item *pNext;
        item(T val, item *pB) : itemValue{val}, pNext{pB} {};
    };

    item* pHead{new item{T(), nullptr}};
    item* pTail{pHead};

public:
    Queue() = default;
    Queue(const Queue&) = delete;
    Queue& operator=(const Queue&) = delete;
    virtual ~Queue();
    void push_back(T value);
    void display();
    T pop_front();
    void insert(int a, T value);
    void remove(int a);
    bool empty();

};

#include "queue.hpp"



#endif //Q2_QUEUE_H
