
#include "queue.h"

template<class T>
Queue<T>::~Queue() {
    item* pTemp{};
    while (pHead->pNext != nullptr){
        pTemp = pHead;
        pHead = pHead->pNext;
        delete pTemp;
    }
    delete pHead;
}

template<class T>
void Queue<T>::push_back(T value) {
    item* pTemp{pTail};
    pTail = new item(value, nullptr);
    pTemp->pNext = pTail;
}

template<class T>
T Queue<T>::pop_front() {
    T val;
    item* ptemp{pHead->pNext};
    pHead->pNext = ptemp->pNext;
    val = ptemp->itemValue;
    delete ptemp;
    return val;
}

template <class T>
void Queue<T>::display() {
    item* pTemp{pHead->pNext};
    int counter{1};
    if(pHead->pNext == nullptr)
        std::cout << "no item!" << std::endl;
    else {
        while (pTemp->pNext != nullptr) {
            std::cout << pTemp->itemValue << ", ";
            pTemp = pTemp->pNext;
            ++counter;
        }
        std::cout << pTemp->itemValue << " -> " << counter << " item" << std::endl;
    }
}

template <class T>
void Queue<T>::insert(int a, T value){
    item* ptemp{pHead->pNext};
    for(size_t i{}; i < a - 1 ; i++){
        ptemp = ptemp->pNext;
    }
    item* pInsert{new item{value, ptemp->pNext}};
    ptemp->pNext = pInsert;
}

template <class T>
void Queue<T>::remove(int a) {
    item* pTemp{pHead->pNext};
    item* pRemove{};
    for(size_t i{}; i < a - 1; i++){
        pTemp = pTemp->pNext;
    }
    pRemove = pTemp->pNext;
    pTemp->pNext = pRemove->pNext;
    delete pRemove;
}

template<class T>
bool Queue<T>::empty() {
    if(pHead->pNext== nullptr)
    return true;
    else
        return false;
}

