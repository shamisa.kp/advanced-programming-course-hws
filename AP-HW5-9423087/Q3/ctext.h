//
// Created by shamisa on 5/12/18.
//

#ifndef Q3_CTEXT_H
#define Q3_CTEXT_H


#include <string>

class CText{
public:
    CText(std::string a);
    ~CText() = default;
    CText(const CText&) = delete;
    CText& operator=(const CText&) = delete;

    std::string getText();


private:
    std::string text;
};


#include "ctext.hpp"

#endif //Q3_CTEXT_H
