//
// Created by shamisa on 5/12/18.
//

#ifndef Q3_STACK_H
#define Q3_STACK_H

#include <memory>
#include <iostream>
#include <cstring>
#include "ctext.h"

template <class T>
class Stack
{
    class item;

    class item{
    public:
        T val;
        item* pNext;
        item(T v, item* pN): val{ v }, pNext{ pN } {};
    };

    item* pTop{};

public:
    Stack() = default;
    virtual ~Stack();
    Stack(const Stack&)= delete;
    Stack& operator=(const Stack&) = delete;


    void push(T val);
    T pop();
    int getCount();
    bool isEmpty();

};


#include "stack.hpp"


#endif //Q3_STACK_H
