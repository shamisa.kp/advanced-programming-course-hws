//
// Created by shamisa on 5/12/18.
//

#include "stack.h"


template<class T>
Stack<T>::~Stack() {
    item* pTemp{};
    while(pTop){
        pTemp = pTop;
        pTop = pTop->pNext;
        delete pTemp;
    }

}

template <class T>
void Stack<T>::push(T val) {
    pTop = new item{val, pTop};
}

template <class T>
T Stack<T>::pop() {
    if(!pTop)
        return nullptr;
    T value = pTop->val;
    item* pTemp = pTop;
    pTop =pTop->pNext;
    delete pTemp;
    return value;
}

template<class T>
bool Stack<T>::isEmpty() {
    return !pTop ? true : false;
}

template<class T>
int Stack<T>::getCount() {
    int count{};
    item* pTemp{pTop};
    while(pTemp){
        pTemp = pTemp->pNext;
        count++;
    }
    return count;
}
