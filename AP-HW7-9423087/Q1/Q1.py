# Shamisa Kaspour 9423087

import re

with open("Q1.txt") as f:
    text = f.readlines()

myStr = str()
for j in range(len(text[-1])):
    for i in range(len(text)):
        try:
            myStr += re.match('([a-zA-Z ])', text[i][j]).string
        except:
            pass

# Remove more than one space
myStr = re.sub(" +", " ", myStr)

print(myStr)
