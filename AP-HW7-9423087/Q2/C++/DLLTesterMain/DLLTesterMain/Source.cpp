/* Shamisa Kaspour 9423087 */

#include <iostream>
#include <chrono>
#include <string>
#include <fstream>

extern "C" {
	__declspec(dllimport) std::string STRFibonacci(std::string s);
	__declspec(dllimport) char* Fibonacci(char* s);
}

int main() {
	std::ifstream Q2File;
	std::ofstream runTimeFile;

	runTimeFile.open("Fibonacci_C++_runtime");
	Q2File.open("Q2.txt");

	std::string num{};
	while (Q2File >> num) {
		auto t0{ std::chrono::high_resolution_clock::now() };
		std::string calculatedFibonacci{ STRFibonacci(num) };
		auto t1{ std::chrono::high_resolution_clock::now() };

		std::cout << num << "\n" << calculatedFibonacci << "\n---------------------\n";
		runTimeFile << num << " " 
			<< std::chrono::duration_cast<std::chrono::nanoseconds>(t1 - t0).count() << "\n";
	}

	Q2File.close();
	runTimeFile.close();
	return 0;
}