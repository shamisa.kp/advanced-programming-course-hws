/* Shamisa Kaspour 9423087 */

#include "mathfuncs.h"
#include <iostream>

std::string STRFibonacci(std::string s) {
	return mathfuncs().Fibonacci(s);
}

char* Fibonacci(char* s) {
	mathfuncs mFuncs{};
	/* Convert to string */
	std::string str{ mFuncs.Fibonacci(std::string(s)) };

	/* Convert to char* */
	char *myCharS = new char[str.length() + 1];
	strcpy_s(myCharS, std::strlen(myCharS), str.c_str());
	return myCharS;
}

mathfuncs::mathfuncs() {

}
mathfuncs::~mathfuncs() {

}

std::string mathfuncs::Fibonacci(std::string s) {
	if (s == "0")
		return "0";
	std::string firstS{ "0" };
	std::string secondS{ "1" };

	int integerS{ std::stoi(s) };
	for (int i{ 1 }; i < integerS; i++) {
		std::string Temp{ addTwoString(firstS, secondS) };
		firstS = secondS;
		secondS = Temp;
	}

	return secondS;
}

/* From StackOverflow */
std::string mathfuncs::addTwoString(std::string& firstStr, std::string& secondStr) {
	/* Some of two STR by the method that we do in paper */
	if (firstStr.length() > secondStr.length())
		swap(firstStr, secondStr);

	/* Final Answer */
	std::string str = "";
	int n1 = firstStr.length(), n2 = secondStr.length();
	int diff = n2 - n1;
	int carry = 0;

	for (int i = n1 - 1; i >= 0; i--) {
		int sum = ((firstStr[i] - '0') +
			(secondStr[i + diff] - '0') +
			carry);
		str.push_back(sum % 10 + '0');
		carry = sum / 10;
	}

	for (int i = n2 - n1 - 1; i >= 0; i--) {
		int sum = ((secondStr[i] - '0') + carry);
		str.push_back(sum % 10 + '0');
		carry = sum / 10;
	}

	if (carry)
		str.push_back(carry + '0');
	reverse(str.begin(), str.end());

	return str;
}
