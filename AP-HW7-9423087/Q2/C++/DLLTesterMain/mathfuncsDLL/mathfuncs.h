/* Shamisa Kaspour 9423087 */

#pragma once
#include <string>

extern "C" {
	__declspec(dllexport) std::string STRFibonacci(std::string s);
	__declspec(dllexport) char* Fibonacci(char* s);
}

class mathfuncs {
public:
	mathfuncs();
	~mathfuncs();
	std::string Fibonacci(std::string sa);

private:
	std::string addTwoString(std::string&, std::string&);
};