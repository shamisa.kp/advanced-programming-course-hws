Shamisa Kaspour 9423087
?
import matplotlib.pyplot as plt
import ctypes as ct
import time


dllPath = r"C:\Users\shamisa\Desktop\HW7\Q2\C++\DLLTesterMain\x64\Debug\mathfuncsDLL.dll"

dll = ct.cdll.LoadLibrary(dllPath)
Fibonacci = dll.Fibonacci
Fibonacci.argtypes = [ct.c_char_p]
Fibonacci.restype = ct.c_char_p

with open(r"C:\Users\shamisa\Desktop\HW7\Q2\C++\DLLTesterMain\DLLTesterMain\Q2.txt", "r") as file:
    lines = file.readlines()
    for line in lines:
        num = line.split()[0]
        t = time.time()
        f = Fibonacci(ct.c_char_p(num.encode('utf-8')))
        t1 = time.time()
        f = f.decode("utf-8")
        print(num +": " + f)
        print("time: {}ns".format((t1 - t)*10**6))

with open(r"C:\Users\shamisa\Desktop\HW7\Q2\C++\DLLTesterMain\DLLTesterMain\Fibonacci_c++_runtime", "r") as file:
    numbers = []
    time = []
    lines = file.readlines()
    for line in lines:
        numbers.append(int(line.split()[0]))
        time.append(int(line.split()[1]))
    plt.plot(numbers, time)
    plt.show()
