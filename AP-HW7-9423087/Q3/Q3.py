# Shamisa Kaspour 9423087

import matplotlib
matplotlib.use("QT5Agg")
import sys
from PyQt5 import uic
import numpy as np
from openpyxl import Workbook, load_workbook
from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QFileDialog
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.ticker import AutoMinorLocator
from matplotlib.figure import Figure
import PyQt5.QtWidgets
import os

Form = uic.loadUiType(os.path.join(os.getcwd(), 'gui.ui'))[0]


class IntroWindow(Form, PyQt5.QtWidgets.QMainWindow):
    def __init__(self):
        Form.__init__(self)
        PyQt5.QtWidgets.QMainWindow.__init__(self)
        self.setupUi(self)

        self.fig = Figure()
        self.ax = self.fig.add_axes([0.1, 0.1, 0.8, 0.8], frameon=False)
        self.ax.axhline(linewidth=3, color="black")
        self.ax.axvline(linewidth=3, color="black")
        self.ax.set_xticks(np.arange(0, 200, 0.8))
        self.ax.set_yticks(np.arange(0, 200, 0.8))
        minorLocator = AutoMinorLocator()
        self.ax.xaxis.set_minor_locator(minorLocator)
        self.ax.yaxis.set_minor_locator(minorLocator)
        self.ax.grid(True)
        self.canvas = FigureCanvas(self.fig)
        self.files = []
        self.x = []
        self.y = []
        self.scatter = self.ax.scatter(self.x, self.y)
        self.xlsxScatter = [self.ax.scatter(self.x, self.y), self.ax.scatter(self.x, self.y)]

        box = QVBoxLayout(self.matplotlibWidget)
        box.addWidget(self.canvas)

        self.addButton.clicked.connect(self.add)
        self.clearButton.clicked.connect(self.clear)
        self.addFileButton.clicked.connect(self.readFile)
        self.plotButton.clicked.connect(self.plot)

    def add(self):
        for iterator in range(len(self.x)):
            if self.x[iterator] == self.xSpinBox.value():
                if self.y[iterator] == self.ySpinBox.value():
                    print("Same Point. Already Added.")
                    return

        self.scatter.remove()
        self.x.append(self.xSpinBox.value())
        self.y.append(self.ySpinBox.value())
        self.scatter = self.ax.scatter(self.x, self.y)

        self.fig.canvas.draw()

    def clear(self):
        # Check Existence
        for iterator in range(len(self.x)):
            if self.x[iterator] == self.xSpinBox.value() and \
                    self.y[iterator] == self.ySpinBox.value():
                # Remove That Element
                self.x.pop(iterator)
                self.y.pop(iterator)
                self.scatter.remove()
                self.scatter = self.ax.scatter(self.x, self.y)
                self.fig.canvas.draw()
                return
        return

    def readFile(self):
        path = QFileDialog.getOpenFileName(self, 'Choose File', "", "*.xlsx")[0]
        if len(self.files) <= 1:
            self.files.append(path)
        else:
            print("You can not add more than 2 files.")

    def plot(self):
        for iterator in range(len(self.files)):
            self.xlsxScatter[iterator].remove()
            myWorkBook = load_workbook(str(self.files[iterator]))
            mySheet = myWorkBook[myWorkBook.sheetnames[0]]

            # Read From File
            firstColumn = []
            secondColumn = []
            # implement DO WHILE
            myIterator = 1
            while True:
                if mySheet['A' + str(myIterator)].value is None:
                    break
                firstColumn.append(mySheet['A' + str(myIterator)].value)
                secondColumn.append(mySheet['B' + str(myIterator)].value)
                myIterator += 1
            self.xlsxScatter[iterator] = self.ax.scatter(firstColumn, secondColumn)
        self.fig.canvas.draw()


# TO WRITE IN A XLSX FILE
x = np.random.normal(0, 1, size=100)
x = (x - x.mean()) / x.std()

y = np.linspace(-5, 5, 100)

book = Workbook()
sheet = book.active

for i in range(len(x)):
    sheet.cell(row=i + 1, column=1).value = x[i]
    sheet.cell(row=i + 1, column=2).value = y[i]

book.save("data.xlsx")

app = PyQt5.QtWidgets.QApplication(sys.argv)
w = IntroWindow()
w.show()
sys.exit(app.exec())
