# Shamisa Kaspour 9423087

import os
from openpyxl import Workbook
import re
import zipfile
import shutil


def isFormatWrong(files):
    # Not End with .zip
    if not files[0].endswith('zip'):
        return True

    # Not Start With AP-HW2-
    if not files[0].startswith(constHWKey):
        return True
    return False


constFinalDirectory = "Categorized_HW2"
wrongFormatStudents = []

# Read Q6
structuredInfo = []
with open(os.path.join(os.getcwd(), "Q6.txt")) as file:
    structuredInfo.append([file.readline().split()[0]])
    lines = file.readlines()
    for line in lines:
        line = line.split()
        for word in line:
            if word == ',':
                line.remove(word)

        for i in range(len(line)):
            if line[i].endswith(':'):
                line[i] = line[i][:-1]
        structuredInfo.append(line)
constHWKey = structuredInfo[0][0]
structuredInfo.pop(0)

# UNZIP AP-HW2.zip
zip_ref = zipfile.ZipFile(os.path.join(os.getcwd(), "AP-HW2.zip"), 'r')
if os.path.exists(os.path.join(os.getcwd(), constHWKey)):
    shutil.rmtree(os.path.join(os.getcwd(), constHWKey))
zip_ref.extractall(os.path.join(os.getcwd(), constHWKey))
zip_ref.close()

# Make Folders
if os.path.exists(os.path.join(os.getcwd(), constFinalDirectory)):
    shutil.rmtree(os.path.join(os.getcwd(), constFinalDirectory))
os.makedirs(os.path.join(os.getcwd(), constFinalDirectory))
for i in range(0, len(structuredInfo)):
    os.makedirs(os.path.join(os.path.join(os.getcwd(), constFinalDirectory), structuredInfo[i][0]))

# Unzip Every HW and Copy Files
for studentHW in os.listdir(os.path.join(os.getcwd(), constHWKey)):
    wrongFormatFlag = False
    filesOfFolder = os.listdir(os.path.join(os.getcwd(), constHWKey, studentHW))
    if isFormatWrong(filesOfFolder):
        wrongFormatFlag = True
    else:

        try:
            # UNZIP AP-HW2-....zip
            path = os.path.join(os.getcwd(), constHWKey, studentHW)
            zip_ref = zipfile.ZipFile(os.path.join(path, filesOfFolder[0]), 'r')
            zip_ref.extractall(path)
            zip_ref.close()

            # Check Any Sub directory Existence
            subDirectoryName = str()
            thereIsNoSubDirectory = False
            for dir in os.listdir(path):
                if os.path.isdir(os.path.join(path, dir)):
                    subDirectoryName = dir
                if dir == structuredInfo[0][0]:
                    thereIsNoSubDirectory = True

            # Student Number
            studentNumber = (filesOfFolder[0])[7:-4]

            # If there was no sub directory
            if not thereIsNoSubDirectory:
                path = os.path.join(path, subDirectoryName)

            # Rename And Copy Files
            canYouCopyThat = False
            for question in structuredInfo:
                if os.path.exists(os.path.join(path, question[0])):
                    for file in os.listdir(os.path.join(path, question[0])):
                        for iterator in range(1, len(question)):
                            if file.endswith(question[iterator]):
                                os.path.join(path, question[0], file)
                                os.path.join(os.getcwd(), constFinalDirectory,
                                             question[0], studentNumber + '_' + file)
                                shutil.copyfile(os.path.join(path, question[0], file),
                                                os.path.join(os.getcwd(), constFinalDirectory, question[0],
                                                             studentNumber + '_' + file))
                                canYouCopyThat = True

                if not canYouCopyThat:
                    wrongFormatFlag = True
        except:
            wrongFormatFlag = True

    if wrongFormatFlag:
        wrongFormatStudents.append(re.sub('_.*$', "", studentHW))  # Just the name

# Excel
wb = Workbook(write_only=True)
ws = wb.create_sheet()
for student in wrongFormatStudents:
    ws.append([student])
wb.save('Wrong Format Students.xlsx')
